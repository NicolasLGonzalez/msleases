FROM cablevision/java

VOLUME /tmp
ARG APP_JAR

EXPOSE 8083

ADD ${APP_JAR} app.jar
ENTRYPOINT ["sh", "-c", "envconsul -consul-addr $CONSUL_ADDR -prefix $APP_ID java -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]