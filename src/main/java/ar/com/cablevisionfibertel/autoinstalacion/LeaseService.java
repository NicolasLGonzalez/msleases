package ar.com.cablevisionfibertel.autoinstalacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class LeaseService extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(LeaseService.class, args);
	}
}
