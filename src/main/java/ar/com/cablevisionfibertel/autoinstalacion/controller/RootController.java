package ar.com.cablevisionfibertel.autoinstalacion.controller;

import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.cablevisionfibertel.autoinstalacion.services.RestClient;

@RestController
public class RootController {
	private static final Logger log = LoggerFactory.getLogger(RootController.class);

	@Autowired
	private RestClient restService;

	@CrossOrigin
	@RequestMapping(value = "/ServiceOrderManagement/leases/lastLease", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getCMInfo(
			@RequestParam(value = "ipAddressCpe", required = false, defaultValue = "") String ipAddressCpe,
			@RequestParam(value = "macAddressCablemodem", required = false, defaultValue = "") String macAddressCablemodem) {

		if (!ipAddressCpe.isEmpty() && !macAddressCablemodem.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Debe enviar un solo parametro");
		}

		if (ipAddressCpe.isEmpty() && macAddressCablemodem.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Debe enviar un parametro");
		}

		log.info("Getting cablemodem connection information of {}",
				!ipAddressCpe.isEmpty() ? ipAddressCpe : macAddressCablemodem);

		try {
			Map<String, String> r;

			r = macAddressCablemodem.isEmpty() ? restService.getCMInfoByIP(ipAddressCpe) : restService.GetCMInfoByMac(macAddressCablemodem);

			log.info("Result -> {}", r);
			return ResponseEntity.ok(r);
		} catch (NoSuchElementException e) {
			log.warn("Lease information missing");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		} catch (IOException | JSONException e) {
			log.error("Could not perform action", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ha ocurrido un error inesperado");
		} catch (IllegalArgumentException e) {
			log.error("Could not perform action", e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	}

}