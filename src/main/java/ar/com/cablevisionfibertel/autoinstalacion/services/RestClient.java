package ar.com.cablevisionfibertel.autoinstalacion.services;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

/**
 * Created by Juan De Croche on 6/7/17.
 */
@Service("AllAround-RestClient")
public class RestClient {

	private static final Logger LOG = LoggerFactory.getLogger(RestClient.class);
	private static final Pattern MAC_PATTERN = Pattern.compile("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$");
	private static final Pattern IP_PATTERN = Pattern
			.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

	@Value("${leases.urlplaceholder.macrelay}")
	private String macrelayPlaceholder;

	@Value("${leases.urlplaceholder.cmipaddr}")
	private String cmIpaddrPlaceHolder;

	@Value("${leases.urlplaceholder.pcipaddr}")
	private String pcIpaddrPlaceHolder;

	@Value("${leases.validity}")
	private long leaseValidity;

	@PostConstruct
	public void config() {
		System.out.println("ENV: " + System.getenv("LEASES_URLPLACEHOLDER_MACRELAY"));
		System.out.println("leases.urlplaceholder.macrelay: " + macrelayPlaceholder);
	}

	/**
	 * @param ip
	 * @return
	 * @throws IOException
	 *             if it fails to call the service
	 * @throws NoSuchElementException
	 *             if there's no match for the ip address
	 * @throws JSONException
	 */
	private Map<String, String> getCMInfo(String ip, String mac)
			throws IOException, NoSuchElementException, JSONException {
		final Map<String, String> res = new HashMap<>();

		final String r1 = doGet(String.format(macrelayPlaceholder, ip), "sr-mongo-xp02.corp.cablevision.com.ar", 8080);

		LOG.debug(r1.toString());
		if (!new JSONObject(r1).has("_embedded")) {
			throw new NoSuchElementException("No existe lease para el ip: " + ip);
		}

		JSONObject ipv4PCLease = new JSONObject(r1).getJSONObject("_embedded").getJSONArray("rh:doc").getJSONObject(0)
				.getJSONObject("ipv4");
		LOG.debug("Checking lease validity");

		final long leaseDate = ipv4PCLease.getJSONObject("date").getLong("$date");
		final long deltaDate = System.currentTimeMillis() - leaseDate;
		LOG.debug("leasedate: {}", leaseDate);
		LOG.debug("curredate: {}", System.currentTimeMillis());
		LOG.debug("delta: 	{} ~{}hs ", deltaDate, Duration.ofMillis(deltaDate).toHours());
		LOG.debug("valdity:	{}", leaseValidity);

		if (deltaDate > leaseValidity) {
			LOG.warn("Lease is old...");
			throw new NoSuchElementException("Por favor, desconecta y volve a conectar el Cablemodem.");
		}

		final String macRelay = ipv4PCLease.getString("macRelay"); // CM mac relay

		res.put("cmMacRelay", macRelay);

		final String r2 = doGet(String.format(cmIpaddrPlaceHolder, macRelay), "sr-mongo-xp02.corp.cablevision.com.ar",
				8080);
		LOG.debug(r2.toString());
		final String cmIpaddr = new JSONObject(r2).getJSONObject("_embedded").getJSONArray("rh:doc").getJSONObject(0)
				.getJSONObject("ipv4").getString("ipAddress");

		res.put("cmIpAddress", cmIpaddr);

		return res;
	}

	public Map<String, String> getCMInfoByIP(String ip)
			throws IllegalArgumentException, NoSuchElementException, IOException, JSONException {
		if (validateIP(ip)) {
			return getCMInfo(ip, null);
		}
		throw new IllegalArgumentException("Formato de IP invalido: " + ip);
	}

	public Map<String, String> GetCMInfoByMac(String mac) throws IllegalArgumentException, JSONException, IOException {
		String ip = "";
		if (validateMAC(mac)) {
			LOG.warn("Received a mac address instead of ip, obtaining PC Ip");
			final String r0 = doGet(String.format(pcIpaddrPlaceHolder, mac), "sr-mongo-xp02.corp.cablevision.com.ar",
					8080);
			LOG.debug(r0.toString());
			ip = new JSONObject(r0).getJSONObject("_embedded").getJSONArray("rh:doc").getJSONObject(0)
					.getJSONObject("ipv4").getString("ipAddress");
			return getCMInfo(ip, mac);
		} else {
			throw new IllegalArgumentException("Formato de MAC invalido: " + ip);
		}

	}

	private String doGet(String url, String host, int port) throws IOException {
		url = url.replace("}", "%7D");
		url = url.replace("{", "%7B");
		url = url.replace("\"", "%22");

		HostConfiguration hc = new HostConfiguration();
		hc.setHost(host, port);
		HttpClient httpClient = new HttpClient();
		httpClient.setHostConfiguration(hc);
		final GetMethod get = new GetMethod(url);
		try {
			httpClient.executeMethod(get);
			final String a = get.getResponseBodyAsString();
			get.releaseConnection();
			return a;
		} catch (IOException e) {
			throw e;
		}
	}

	private boolean validateMAC(final String mac) {
		return MAC_PATTERN.matcher(mac).matches();
	}

	private boolean validateIP(final String ip) {
		return IP_PATTERN.matcher(ip).matches();
	}
}
